Description
-----------

This repository holds the source codes that reproduce the experimental results 
of "Classification of remote sensing data with morphological attributes profiles: a decade of advances".
Experiments were performed using the open-source Simple Attribute Profile (SAP) (https://gitlab.inria.fr/fguiotte/sap)
and Scikit-learn libraries.


Experiments with Gray-Potsdam dataset
-------------------------------------

In order to reproduce the experimental results with Gray-Potsdam, the Potsdam dataset should be
downloaded from http://www2.isprs.org/commissions/comm3/wg4/2d-sem-label-potsdam.html.

Then, the files 'top_potsdam_7_7_RGB.tif' and './Data/top_potsdam_7_7_label.tif' should be placed in the folder "./Data/Potsdam/".

Finally, the following codes can be executed depending on the chosen partition of Gray-Potsdam:
    
	GrayPotsdam1 and GrayPotsdam2: potsdam_gray_profiles.py [method] [connectivity] [training_set]
	GrayPotsdam3 (one tree): potsdam_gray_profiles_generalization_1.py [method] [connectivity] [training_set]
	GrayPotsdam3 (two trees): potsdam_gray_profiles_generalization_2.py [method] [connectivity] [training_set]

    	[method] = GRAY, AP, MAX, MIN, SDAP, LFAP, LFSDAP, ALPHA, OMEGA, FP_AREA, FP_MEAN, FP_AREA_MEAN

    	[connectivity] = 4, 8
 
    	[training_set] = potsdam_training_60000_per_class.npy (GrayPotsdam1)
	                 potsdam_training_60000_per_class_50_per_cent_regions.npy (GrayPotsdam2)
	                 potsdam_training_60000_per_class_3000_lines.npy (GrayPotsdam3)
                     
With 'potsdam_gray_profiles.py', the accuracy scores and classification maps are saved respectively in 
'potsdam_[connectivity]_[method].txt' and 'classification_potsdam_[connectivity]_[method].png.

With 'potsdam_gray_profiles_generalization_1.py' the accuracy scores and classification maps are saved respectively in 
'potsdam_[connectivity]_[method]_generalization_1.txt' and 'classification_potsdam_[connectivity]_[method]_generalization_1.png.

With 'potsdam_gray_profiles_generalization_2.py' the accuracy scores and classification maps are saved respectively in 
'potsdam_[connectivity]_[method]_generalization_2.txt' and 'classification_potsdam_[connectivity]_[method]_generalization_2.png.


Experiments with Pavia dataset
------------------------------

In order to reproduce the experimental results with Pavia dataset, the following line can be executed:

	pavia_profiles.py [method] [connectivity] [quantization] [training_set]

	[method] = GRAY, AP, MAX, MIN, SDAP, LFAP, LFSDAP, HAP, HSDAP, ALPHA, OMEGA, FP_AREA, FP_MEAN, FP_AREA_MEAN

    	[connectivity] = 4, 8

    	[quantization] = 8, 16, 64

	[training_set] = STANDARD, NEW 
