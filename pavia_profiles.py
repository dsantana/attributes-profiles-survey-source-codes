# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 19:11:07 2020

@author: Deise Santana Maia
"""

import pandas as pd
from sklearn.datasets import fetch_mldata
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns

import numpy as np
import matplotlib.pyplot as plt
import higra as hg
import scipy.io as sio
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from ClassificationEvaluation import classificationEvaluation
import sap
from PIL import Image
from matplotlib import colors
import sys
import time


################################################################################################################

def compute_AP(image, lamb_area, lamb_moi, adj):
    nb_thresholds_area = len(lamb_area)
    nb_thresholds_moi = len(lamb_moi)
    AP_area = np.zeros((nb_thresholds_area*2+1,image.shape[0],image.shape[1],4))
    AP_moi = np.zeros((nb_thresholds_moi*2+1,image.shape[0],image.shape[1],4))

    for i in range(0,4):
        AP_area[:,:,:,i] = sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj).data
        AP_moi[:,:,:,i] = sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive').data
    final_AP = np.concatenate([AP_area[:,:,:,0],AP_moi[:,:,:,0]],axis=0)
    for i in range(1,4):
        final_AP = np.concatenate([final_AP,AP_area[:,:,:,i],AP_moi[:,:,:,i]],axis=0)    

    return final_AP

def compute_MAX(image, lamb_area, lamb_moi, adj):
    nb_thresholds_area = len(lamb_area)
    nb_thresholds_moi = len(lamb_moi)
    AP_area = np.zeros((nb_thresholds_area*2+1,image.shape[0],image.shape[1],4))
    AP_moi = np.zeros((nb_thresholds_moi*2+1,image.shape[0],image.shape[1],4))

    for i in range(0,4):
        AP_area[:,:,:,i] = sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj).data
        AP_moi[:,:,:,i] = sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive').data
    final_AP_max = np.concatenate([AP_area[nb_thresholds_area:nb_thresholds_area*2+1,:,:,0],AP_moi[nb_thresholds_moi:nb_thresholds_moi*2+1,:,:,0]],axis=0)
    for i in range(1,4):
        final_AP_max = np.concatenate([final_AP_max,AP_area[nb_thresholds_area:nb_thresholds_area*2+1,:,:,i],AP_moi[nb_thresholds_moi:nb_thresholds_moi*2+1,:,:,i]],axis=0)

    return final_AP_max

def compute_MIN(image, lamb_area, lamb_moi, adj):
    nb_thresholds_area = len(lamb_area)
    nb_thresholds_moi = len(lamb_moi)
    AP_area = np.zeros((nb_thresholds_area*2+1,image.shape[0],image.shape[1],4))
    AP_moi = np.zeros((nb_thresholds_moi*2+1,image.shape[0],image.shape[1],4))

    for i in range(0,4):
        AP_area[:,:,:,i] = sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj).data
        AP_moi[:,:,:,i] = sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive').data
    final_AP_min = np.concatenate([AP_area[0:nb_thresholds_area+1,:,:,0],AP_moi[0:nb_thresholds_moi+1,:,:,0]],axis=0)
    for i in range(1,4):
        final_AP_min = np.concatenate([final_AP_min,AP_area[0:nb_thresholds_area+1,:,:,i],AP_moi[0:nb_thresholds_moi+1,:,:,i]],axis=0)

    return final_AP_min


def compute_SDAP(image, lamb_area, lamb_moi, adj):
    SDAP_area = sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,0]), {'area': lamb_area}, adjacency=adj)
    SDAP_moi = sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,0]), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
    SDAP = sap.concatenate((SDAP_area,SDAP_moi))
    
    for i in range(1,4):
        SDAP_area = sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj)
        SDAP_moi = sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
        SDAP = sap.concatenate((SDAP,SDAP_area,SDAP_moi))
    
    final_SDAP = sap.vectorize(SDAP)
    
    return final_SDAP

def compute_ALPHA(image, lamb_area, lamb_moi, adj):
    ALPHA_area = sap.profiles.alpha_profiles(np.ascontiguousarray(image[:,:,0]), {'area': lamb_area}, adjacency=adj)
    ALPHA_moi = sap.profiles.alpha_profiles(np.ascontiguousarray(image[:,:,0]), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
   
    ALPHA_profile = sap.concatenate((ALPHA_area,ALPHA_moi))
    
    for i in range(1,4):
        ALPHA_area = sap.profiles.alpha_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj)
        ALPHA_moi = sap.profiles.alpha_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
        ALPHA_profile = sap.concatenate((ALPHA_profile,ALPHA_area,ALPHA_moi))
    
    final_ALPHA = sap.vectorize(ALPHA_profile)
    
    return final_ALPHA

def compute_OMEGA(image, lamb_area, lamb_moi, adj):
    OMEGA_area = sap.profiles.omega_profiles(np.ascontiguousarray(image[:,:,0]), {'area': lamb_area}, adjacency=adj)
    OMEGA_moi = sap.profiles.omega_profiles(np.ascontiguousarray(image[:,:,0]), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
    OMEGA_profile = sap.concatenate((OMEGA_area,OMEGA_moi))
    
    for i in range(1,4):
        OMEGA_area = sap.profiles.omega_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj)
        OMEGA_moi = sap.profiles.omega_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
        OMEGA_profile = sap.concatenate((OMEGA_profile,OMEGA_area,OMEGA_moi))
    
    final_OMEGA = sap.vectorize(OMEGA_profile)
    
    return final_OMEGA

def compute_LFAP(image, lamb_area, lamb_moi, adj):
    AP = sap.attribute_profiles(np.ascontiguousarray(image[:,:,0]), {'area': lamb_area}, adjacency=adj)
    for i in range(1,4):
        AP = sap.concatenate((AP, sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj)))
    for i in range(0,4):
        AP = sap.concatenate((AP, sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive')))
    
    LFAP = sap.local_features(AP, local_feature=(np.mean, np.std), patch_size=7) #patch_size changed from 5 to 7 to match with the FP article
    LFAP = sap.vectorize(LFAP)
    
    return LFAP

def compute_LFSDAP(image, lamb_area, lamb_moi, adj):
    SDAP = sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,0]), {'area': lamb_area}, adjacency=adj)
    for i in range(1,4):
        SDAP = sap.concatenate((SDAP, sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj)))
    for i in range(0,4):
        SDAP = sap.concatenate((SDAP, sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive')))
    
    LFSDAP = sap.local_features(SDAP, local_feature=(np.mean, np.std), patch_size=7) #patch_size changed from 5 to 7 to match with the FP article
    LFSDAP = sap.vectorize(LFSDAP)
    
    return LFSDAP

def compute_HAP(image, lamb_area, lamb_moi, adj):
    d1 = image.shape[0]
    d2 = image.shape[1]
    AP = sap.attribute_profiles(np.ascontiguousarray(image[:,:,0]), {'area': lamb_area}, adjacency=adj)
    for i in range(1,4):
        AP = sap.concatenate((AP, sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj)))
    for i in range(0,4):
        AP = sap.concatenate((AP, sap.attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive')))
        
    nbins = 7 # number of bins
    w = 3 # local window size: 7x7
    AP_padding=np.pad(AP, w, 'reflect')[w:AP.shape[0]+w,:,:]

    HAP = np.zeros((nbins*AP.shape[0],d1,d2))
    for k in range(0,HAP.shape[0]):
        AP_temp = AP_padding[k,:,:]
        vmin = np.amin(AP_temp)
        vmax = np.amax(AP_temp)
        for i in range(w,d1+w):
            for j in range(w,d2+w):
                patch_ij = AP_temp[i-w:i+w+1,j-w:j+w+1]
                HAP[k*nbins:(k+1)*nbins,i-w,j-w] = np.histogram(patch_ij,bins=nbins,range=(vmin,vmax))[0]
    
    return HAP
    
def compute_HSDAP(image, lamb_area, lamb_moi, adj):
    d1 = image.shape[0]
    d2 = image.shape[1]
    SDAP = sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,0]), {'area': lamb_area}, adjacency=adj)
    for i in range(1,4):
        SDAP = sap.concatenate((SDAP, sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, adjacency=adj)))
    for i in range(0,4):
        SDAP = sap.concatenate((SDAP, sap.self_dual_attribute_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive')))
        
    nbins = 7 # number of bins
    w = 3 # local window size: 7x7
    SDAP_padding=np.pad(SDAP, w, 'reflect')[w:SDAP.shape[0]+w,:,:]

    HSDAP = np.zeros((nbins*SDAP.shape[0],d1,d2))
    for k in range(0,HSDAP.shape[0]):
        SDAP_temp = SDAP_padding[k,:,:]
        vmin = np.amin(SDAP_temp)
        vmax = np.amax(SDAP_temp)
        for i in range(w,d1+w):
            for j in range(w,d2+w):
                patch_ij = SDAP_temp[i-w:i+w+1,j-w:j+w+1]
                HSDAP[k*nbins:(k+1)*nbins,i-w,j-w] = np.histogram(patch_ij,bins=nbins,range=(vmin,vmax))[0]
    
    return HSDAP

def compute_FP(image, lamb_area, lamb_moi, adj, method):
    if (method == "FP_AREA_MEAN"):
        out_feature = {'mean_vertex_weights','area'}
    elif (method == "FP_AREA"):
        out_feature = {'area'}
    elif (method == "FP_MEAN"):
        out_feature = {'mean_vertex_weights'}
        
    FP_area = sap.feature_profiles(np.ascontiguousarray(image[:,:,0]), {'area': lamb_area}, out_feature=out_feature,adjacency=adj)
    FP_moi = sap.feature_profiles(np.ascontiguousarray(image[:,:,0]), {'moment_of_inertia': lamb_moi}, adjacency=adj, out_feature=out_feature,filtering_rule='subtractive')
    FP = sap.concatenate((FP_area,FP_moi))
    for i in range(1,4):
        FP_area = sap.feature_profiles(np.ascontiguousarray(image[:,:,i]), {'area': lamb_area}, out_feature=out_feature, adjacency=adj)
        FP_moi = sap.feature_profiles(np.ascontiguousarray(image[:,:,i]), {'moment_of_inertia': lamb_moi}, adjacency=adj, out_feature=out_feature,filtering_rule='subtractive')
        FP = sap.concatenate((FP,FP_area,FP_moi))
        
    final_FP = sap.vectorize(FP)
    
    return final_FP
    
################################################################################################################   

def main():
    method = sys.argv[1]
    adj = int(sys.argv[2])
    quantization = int(sys.argv[3])
    training_set = sys.argv[4]

    file = open("pavia_"+str(adj)+"_"+method+".txt",'w')
    
    paviaU_mat = sio.loadmat('./Data/PaviaU/PaviaU.mat')
    paviaU_image = paviaU_mat['paviaU']

    d1 = paviaU_image.shape[0]
    d2 = paviaU_image.shape[1]
    d3 = paviaU_image.shape[2]
    paviaU_image_reshape = np.reshape(paviaU_image,(d1*d2, d3))
    
    pca = PCA()
    pca.fit(paviaU_image_reshape)
    imgR = np.dot(paviaU_image,np.transpose(pca.components_))
    imgR = np.reshape(imgR,(d1,d2,d3))
    img_PCA_ = imgR[:,:,0:4]
    
    #Normalization
    delta = 1 / (np.amax(img_PCA_) - np.amin(img_PCA_))
    img_PCA  = img_PCA_ * delta + (-np.amin(img_PCA_)*delta)

    if (quantization == 8):
        img_PCA = np.double(np.round(img_PCA*255))
    elif (quantization == 16):
        img_PCA = np.double(np.round(img_PCA*65535))

    
    img_gt = sio.loadmat('./Data/PaviaU/img_gt.mat')['img_gt']
    img_train = sio.loadmat('./Data/PaviaU/img_train.mat')['img_train']
    img_gt = img_gt.astype(np.float)
    img_train = img_train.astype(np.float)
    if (training_set == "NEW" or training_set == "New" or training_set == "new"):
        img_train = np.load("./Data/PaviaU/random_training_new_partition.npy")
    nb_samples_train = int(np.sum(img_train>0))
    nb_samples_test = np.sum(np.maximum(img_gt,img_train)>0)-nb_samples_train
    
    cmap = colors.ListedColormap([(0,0,0,1),(0,0,1,1),(0,0.3,1,1),(0,1,1,1),(0.3,1,0.7,1),(0,1,0,1),(0.7,1,0.3,1),(1,1,0,1),(1,0.3,0,1),(1,0,0,1)])
    
    lamb_area=[770, 1538, 2307, 3076, 3846, 4615, 5384, 6153, 6923, 7692, 8461, 9230, 10000, 10769]
    lamb_moi = [0.2, 0.3, 0.4, 0.5]

    ################################################################################################################
    
    start_time=time.time()
    
    if   (method == "GRAY"):
        features = img_PCA        
    elif (method == "AP"):
        features=compute_AP(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "MAX"):
        features=compute_MAX(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "MIN"):
        features=compute_MIN(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "SDAP"):
        features=compute_SDAP(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "LFAP"):
        features=compute_LFAP(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "LFSDAP"):
        features=compute_LFSDAP(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "HAP"):
        features=compute_HAP(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "HSDAP"):
        features=compute_HSDAP(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "ALPHA"):
        features=compute_ALPHA(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "OMEGA"):
        features=compute_OMEGA(img_PCA, lamb_area, lamb_moi, adj)
    elif (method == "FP_AREA" or method == "FP_AREA_MEAN" or method == "FP_MEAN"):
        features=compute_FP(img_PCA, lamb_area, lamb_moi, adj, method)
    else:
        print("Method not implemented!")
        return
                    

    X_train = np.zeros((nb_samples_train,features.shape[0]))
    y_train = np.zeros((nb_samples_train))
    
    X_test = np.zeros((nb_samples_test,features.shape[0]))
    y_test = np.zeros((nb_samples_test))
    
    k = 0
    kk = 0
    for i in range(0,d1):
        for j in range(0,d2):
            if (img_train[i,j] != 0):
                X_train[k] = features[:,i,j]
                y_train[k] = img_train[i,j]
                k=k+1
            if (img_train[i,j] == 0 and img_gt[i,j] != 0):
                X_test[kk] = features[:,i,j]
                y_test[kk] = img_gt[i,j]
                kk=kk+1
 
    clf = RandomForestClassifier(n_estimators=100, random_state=0)
    clf.fit(X_train, y_train)
    label_classif = clf.predict(X_test)

    
    OCA,kappa,perclass,ACA,ConfMat = classificationEvaluation(y_test,label_classif)
    file.write("("+ str(y_train.size)+ " training samples, "+str(y_test.size)+" testing samples, "+str(features.shape[0])+" features)"+"\n")
    file.write("OCA, ACA and Kappa: "+str(round(OCA*100,2))+" & "+str(round(ACA*100,2))+" & "+str(round(kappa,4))+"\n")
    file.write("Accuracy per class: " + str(round(perclass[0]*100,2)) + " & " + str(round(perclass[1]*100,2)) + " & " + 
                str(round(perclass[2]*100,2)) + " & " +str(round(perclass[3]*100,2)) + " & " + str(round(perclass[4]*100,2)) + " & " +\
                str(round(perclass[5]*100,2)) + "\n")
    
    
    print("("+ str(y_train.size)+ " training samples, "+str(y_test.size)+" testing samples, "+str(features.shape[0])+" features)"+"\n")
    print("OCA, ACA and Kappa: "+str(round(OCA*100,2))+" & "+str(round(ACA*100,2))+" & "+str(round(kappa,4))+"\n")
    
    
    total_time = time.time()-start_time
    file.write("Total time : "+ str(total_time)+"\n")
    print("Total time : "+ str(total_time)+"\n")
    
    
    vis_test = np.zeros((d1,d2))
    kk = 0
    for i in range(0, d1):
        for j in range(0, d2):
            if (img_train[i,j] == 0 and img_gt[i,j] != 0):
                vis_test[i,j] = label_classif[kk]
                kk = kk+1
    
    
    plt.imsave("classification_pavia_"+str(adj)+"_"+method+".png",vis_test,cmap=cmap)
    plt.imshow(vis_test,cmap=cmap)
    plt.show()
    
if __name__ == "__main__":
    main()
    
