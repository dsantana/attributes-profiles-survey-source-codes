# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 10:30:20 2020

@author: Deise Santana Maia
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from sklearn.ensemble import RandomForestClassifier
from ClassificationEvaluation import classificationEvaluation
import sap
from PIL import Image
import time
import sys

################################################################################################################

def compute_AP(image, lamb_area, lamb_moi, adj):
    AP_area = sap.attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj).data
    AP_moi = sap.attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive').data

    final_AP = np.concatenate([AP_area,AP_moi],axis=0)
    
    return final_AP

def compute_MAX(image, lamb_area, lamb_moi, adj):
    nb_thresholds_area = len(lamb_area)
    nb_thresholds_moi = len(lamb_moi)

    AP_area = sap.attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj).data
    AP_moi = sap.attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive').data

    final_AP_max = np.concatenate([AP_area[nb_thresholds_area:nb_thresholds_area*2+1,:,:],AP_moi[nb_thresholds_moi:nb_thresholds_moi*2+1,:,:]],axis=0)
    
    return final_AP_max

def compute_MIN(image, lamb_area, lamb_moi, adj):
    nb_thresholds_area = len(lamb_area)
    nb_thresholds_moi = len(lamb_moi)
        
    AP_area = sap.attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj).data
    AP_moi = sap.attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive').data

    final_AP_min = np.concatenate([AP_area[0:nb_thresholds_area+1,:,:],AP_moi[0:nb_thresholds_moi+1,:,:]],axis=0)
    
    return final_AP_min

def compute_SDAP(image, lamb_area, lamb_moi, adj):
    SDAP_area = sap.self_dual_attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    SDAP_moi = sap.self_dual_attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
    SDAP = sap.concatenate((SDAP_area,SDAP_moi))
    
    final_SDAP = sap.vectorize(SDAP)
    
    return final_SDAP

def compute_ALPHA(image, lamb_area, lamb_moi, adj):
    ALPHA_area = sap.profiles.alpha_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    ALPHA_moi = sap.profiles.alpha_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
    ALPHA_profile = sap.concatenate((ALPHA_area,ALPHA_moi))

    final_ALPHA = sap.vectorize(ALPHA_profile)
    
    return final_ALPHA

def compute_OMEGA(image, lamb_area, lamb_moi, adj):
    OMEGA_area = sap.profiles.omega_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    OMEGA_moi = sap.profiles.omega_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')

    OMEGA_profile = sap.concatenate((OMEGA_area,OMEGA_moi))

    final_OMEGA = sap.vectorize(OMEGA_profile)
    
    return final_OMEGA

def compute_LFAP(image, lamb_area, lamb_moi, adj):
    AP = sap.attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    AP = sap.concatenate((AP, sap.attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive')))
    
    LFAP = sap.local_features(AP, local_feature=(np.mean, np.std), patch_size=7)
    LFAP = sap.vectorize(LFAP)
    
    return LFAP

def compute_LFSDAP(image, lamb_area, lamb_moi, adj):
    SDAP_area = sap.self_dual_attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    SDAP_moi = sap.self_dual_attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
    SDAP = sap.concatenate((SDAP_area,SDAP_moi))

    LFSDAP = sap.local_features(SDAP, local_feature=(np.mean, np.std), patch_size=7)
    LFSDAP = sap.vectorize(LFSDAP)
    
    return LFSDAP

def compute_FP(image, lamb_area, lamb_moi, adj, method):
    if (method == "FP_AREA_MEAN"):
        out_feature = {'mean_vertex_weights','area'}
    elif (method == "FP_AREA"):
        out_feature = {'area'}
    elif (method == "FP_MEAN"):
        out_feature = {'mean_vertex_weights'}
    FP_area = sap.feature_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj, out_feature=out_feature)
    FP_moi = sap.feature_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj, out_feature=out_feature, filtering_rule='subtractive')
    FP = sap.concatenate((FP_area,FP_moi))

    final_FP = sap.vectorize(FP)
    
    return final_FP

    
################################################################################################################   

def main():
    method = sys.argv[1]
    adj = int(sys.argv[2])
    training_set = sys.argv[3]
    
    file = open("potsdam_"+str(adj)+"_"+method+".txt",'w')
    
    potsdam_rgb = Image.open('./Data/Potsdam/top_potsdam_7_7_RGB.tif')
    potsdam_rgb = np.array(potsdam_rgb)
    potsdam_gray = np.round(potsdam_rgb[:,:,0]*0.3 + potsdam_rgb[:,:,1]*0.59 + potsdam_rgb[:,:,2]*0.11)
    potsdam_gray = np.array(potsdam_gray)
    d1 = potsdam_gray.shape[0]
    d2 = potsdam_gray.shape[1]
    
    potsdam_label = Image.open('./Data/Potsdam/top_potsdam_7_7_label.tif')
    potsdam_label = np.array(potsdam_label)/255
    potsdam_label = potsdam_label[:,:,0] + 2*potsdam_label[:,:,1] + 4*potsdam_label[:,:,2]
    aux1 = potsdam_label == 6
    aux2 = potsdam_label == 7
    aux = aux1.astype(int)+aux2.astype(int)
    potsdam_label = potsdam_label-aux
    
    # Color map corresponding to the colors of the GT data
    #cmap = colors.ListedColormap([(0,0,0,1),(1,0,0,1),(0,1,0,1),(1,1,0,1),(0,0,1,1),(0,1,1,1),(1,1,1,1)])
    cmap = colors.ListedColormap([(1,0,0,1),(0,1,0,1),(1,1,0,1),(0,0,1,1),(0,1,1,1),(1,1,1,1)])
    unique,counts=np.unique(potsdam_label,return_counts=True)
    #print(unique,counts)
    
    cmap_ = colors.ListedColormap([(1,0,0,1),(0,1,0,1),(1,1,0,1),(0,0,1,1),(0,1,1,1),(1,1,1,1)])
    #plt.figure("GT")
    #plt.imshow(potsdam_label,cmap=cmap_)
    #plt.axis('off')
    #plt.show()
    
    ################################################################################################################
    ############################################ Random training set ###############################################
    ################################################################################################################
    
    random_potsdam_training = np.load(training_set)
    nb_samples_train = int(np.sum(random_potsdam_training>0))
    
    unique,counts = np.unique(random_potsdam_training,return_counts=True)
    print("Random training set (number of samples per class):", counts, unique)
    
    lamb_area=[200, 400, 800, 1600, 3200, 6400, 12800, 25600, 51200, 102400, 204800, 409600, 819200, 1638400]
    lamb_moi = [0.2, 0.3, 0.4, 0.5]
    
    ################################################################################################################
    start_time=time.time()
    
    if   (method == "GRAY"):
        features = np.zeros((1,d1,d2))
        features[0,:,:] = potsdam_gray        
    elif (method == "AP"):
        features=compute_AP(potsdam_gray, lamb_area, lamb_moi, adj)
    elif (method == "MAX"):
        features=compute_MAX(potsdam_gray, lamb_area, lamb_moi, adj)
    elif (method == "MIN"):
        features=compute_MIN(potsdam_gray, lamb_area, lamb_moi, adj)
    elif (method == "SDAP"):
        features=compute_SDAP(potsdam_gray, lamb_area, lamb_moi, adj)
    elif (method == "LFAP"):
        features=compute_LFAP(potsdam_gray, lamb_area, lamb_moi, adj)
    elif (method == "LFSDAP"):
        features=compute_LFSDAP(potsdam_gray, lamb_area, lamb_moi, adj)
    elif (method == "ALPHA"):
        features=compute_ALPHA(potsdam_gray, lamb_area, lamb_moi, adj)
    elif (method == "OMEGA"):
        features=compute_OMEGA(potsdam_gray, lamb_area, lamb_moi, adj)
    elif (method == "FP_AREA" or method == "FP_AREA_MEAN" or method == "FP_MEAN"):
        features=compute_FP(potsdam_gray, lamb_area, lamb_moi, adj, method)
    elif (method == "NO_THRESHOLD"):
        features=compute_NO_THRESHOLD(potsdam_gray,adj)
    else:
        print("Method not implemented!")
        return
                    

    X_train = np.zeros((nb_samples_train,features.shape[0]))
    y_train = np.zeros((nb_samples_train))
    nb_samples_test = d1*d2-nb_samples_train
    
    X_test = np.zeros((nb_samples_test,features.shape[0]))
    y_test = np.zeros((nb_samples_test))
    
    k = 0
    kk = 0
    for i in range(0,d1):
        for j in range(0,d2):
            if (random_potsdam_training[i,j] != 0):
                X_train[k] = features[:,i,j]
                y_train[k] = potsdam_label[i,j]
                k=k+1
            else:
                X_test[kk] = features[:,i,j]
                y_test[kk] = potsdam_label[i,j]
                kk=kk+1
 
    clf = RandomForestClassifier(n_estimators=100, random_state=0)
    clf.fit(X_train, y_train.ravel())
    label_classif = clf.predict(X_test)

    
    OCA,kappa,perclass,ACA,ConfMat = classificationEvaluation(y_test,label_classif)
    file.write("("+ str(y_train.size)+ " training samples, "+str(y_test.size)+"testing samples)"+"\n")
    file.write("OCA, ACA and Kappa: "+str(round(OCA*100,2))+" & "+str(round(ACA*100,2))+" & "+str(round(kappa,4))+"\n")
    file.write("Accuracy per class: " + str(round(perclass[0]*100,2)) + " & " + str(round(perclass[1]*100,2)) + " & " + 
                str(round(perclass[2]*100,2)) + " & " +str(round(perclass[3]*100,2)) + " & " + str(round(perclass[4]*100,2)) + " & " +\
                str(round(perclass[5]*100,2)) + "\n")
    
    
    print("("+ str(y_train.size)+ " training samples, "+str(y_test.size)+"testing samples)"+"\n")
    print("OCA, ACA and Kappa: "+str(round(OCA*100,2))+" & "+str(round(ACA*100,2))+" & "+str(round(kappa,4))+"\n")
    
    
    total_time = time.time()-start_time
    file.write("Total time to compute "+ str(total_time)+"\n")
    print("Total time to compute "+ str(total_time)+"\n")
    
    
    vis_test = np.zeros((d1,d2))
    kk = 0
    for i in range(0, d1):
        for j in range(0, d2):
            if (random_potsdam_training[i,j] != 0):
                vis_test[i,j] = potsdam_label[i,j]
            if (random_potsdam_training[i,j] == 0):
                vis_test[i,j] = label_classif[kk]
                kk = kk+1
    
    
    plt.imsave("classification_potsdam_"+str(adj)+"_"+method+".png",vis_test,cmap=cmap)    
    
if __name__ == "__main__":
    main()
    
