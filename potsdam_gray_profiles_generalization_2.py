# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 10:30:20 2020

@author: Deise Santana Maia
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from sklearn.ensemble import RandomForestClassifier
from ClassificationEvaluation import classificationEvaluation
import sap
from PIL import Image
import pandas as pd
import time
import sys

################################################################################################################

def compute_AP(image, lamb_area, lamb_moi, adj):
    AP_area = sap.attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj).data
    AP_moi = sap.attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive').data

    final_AP = np.concatenate([AP_area,AP_moi],axis=0)
    
    return final_AP

def compute_MAX(image, lamb_area, lamb_moi, adj):
    nb_thresholds_area = len(lamb_area)
    nb_thresholds_moi = len(lamb_moi)

    AP_area = sap.attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj).data
    AP_moi = sap.attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive').data

    final_AP_max = np.concatenate([AP_area[nb_thresholds_area:nb_thresholds_area*2+1,:,:],AP_moi[nb_thresholds_moi:nb_thresholds_moi*2+1,:,:]],axis=0)
    
    return final_AP_max

def compute_MIN(image, lamb_area, lamb_moi, adj):
    nb_thresholds_area = len(lamb_area)
    nb_thresholds_moi = len(lamb_moi)
        
    AP_area = sap.attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj).data
    AP_moi = sap.attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive').data

    final_AP_min = np.concatenate([AP_area[0:nb_thresholds_area+1,:,:],AP_moi[0:nb_thresholds_moi+1,:,:]],axis=0)
    
    return final_AP_min

def compute_SDAP(image, lamb_area, lamb_moi, adj):
    SDAP_area = sap.self_dual_attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    SDAP_moi = sap.self_dual_attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
    SDAP = sap.concatenate((SDAP_area,SDAP_moi))
    
    final_SDAP = sap.vectorize(SDAP)
    
    return final_SDAP

def compute_ALPHA(image, lamb_area, lamb_moi, adj):
    ALPHA_area = sap.profiles.alpha_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    ALPHA_moi = sap.profiles.alpha_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
    ALPHA_profile = sap.concatenate((ALPHA_area,ALPHA_moi))

    final_ALPHA = sap.vectorize(ALPHA_profile)
    
    return final_ALPHA

def compute_OMEGA(image, lamb_area, lamb_moi, adj):
    OMEGA_area = sap.profiles.omega_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    OMEGA_moi = sap.profiles.omega_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')

    OMEGA_profile = sap.concatenate((OMEGA_area,OMEGA_moi))

    final_OMEGA = sap.vectorize(OMEGA_profile)
    
    return final_OMEGA

def compute_LFAP(image, lamb_area, lamb_moi, adj):
    AP = sap.attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    AP = sap.concatenate((AP, sap.attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi},adjacency=adj,filtering_rule='subtractive')))
    
    LFAP = sap.local_features(AP, local_feature=(np.mean, np.std), patch_size=7)
    LFAP = sap.vectorize(LFAP)
    
    return LFAP

def compute_LFSDAP(image, lamb_area, lamb_moi, adj):
    SDAP_area = sap.self_dual_attribute_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj)
    SDAP_moi = sap.self_dual_attribute_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj,filtering_rule='subtractive')
    SDAP = sap.concatenate((SDAP_area,SDAP_moi))

    LFSDAP = sap.local_features(SDAP, local_feature=(np.mean, np.std), patch_size=7)
    LFSDAP = sap.vectorize(LFSDAP)
    
    return LFSDAP

def compute_FP(image, lamb_area, lamb_moi, adj, method):
    if (method == "FP_AREA_MEAN"):
        out_feature = {'mean_vertex_weights','area'}
    elif (method == "FP_AREA"):
        out_feature = {'area'}
    elif (method == "FP_MEAN"):
        out_feature = {'mean_vertex_weights'}
    FP_area = sap.feature_profiles(np.ascontiguousarray(image), {'area': lamb_area}, adjacency=adj, out_feature=out_feature)
    FP_moi = sap.feature_profiles(np.ascontiguousarray(image), {'moment_of_inertia': lamb_moi}, adjacency=adj, out_feature=out_feature, filtering_rule='subtractive')
    FP = sap.concatenate((FP_area,FP_moi))

    final_FP = sap.vectorize(FP)
    
    return final_FP

def compute_NO_THRESHOLD(image, adj):
    no_threshold_area_max=np.loadtxt(fname = "./Data/2_Ortho_RGB/No_threshold_complete/potsdamEntireAreaNoThreshold"+str(adj)+"C_MAX.dat",delimiter=',',usecols=range(6))
    no_threshold_moi_max=np.loadtxt(fname = "./Data/2_Ortho_RGB/No_threshold_complete/potsdamEntireMomentNoThreshold"+str(adj)+"C_MAX.dat",delimiter=',',usecols=range(6))
    no_threshold_area_min=np.loadtxt(fname = "./Data/2_Ortho_RGB/No_threshold_complete/potsdamEntireAreaNoThreshold"+str(adj)+"C_MIN.dat",delimiter=',',usecols=range(6))
    no_threshold_moi_min=np.loadtxt(fname = "./Data/2_Ortho_RGB/No_threshold_complete/potsdamEntireMomentNoThreshold"+str(adj)+"C_MIN.dat",delimiter=',',usecols=range(6))

    nb_features = 18
    d1 = image.shape[0]
    d2 = image.shape[1]
    
    NO_THRESH = np.array((nb_features,d1,d2))
    number_of_pixels = d1*d2
    for i in range(0,number_of_pixels):
        x = int(no_threshold_area_max[i,0])
        y = int(no_threshold_area_max[i,1])
        NO_THRESH[:,y,x] = np.concatenate([no_threshold_area_max[i,2:6],no_threshold_area_min[i,2:6],[image[y,x]],
                                      no_threshold_moi_max[i,2:6],no_threshold_moi_min[i,2:6],[image[y,x]]])
    return NO_THRESH


    
################################################################################################################   

def main():
    method = sys.argv[1]
    adj = int(sys.argv[2])
    training_set = sys.argv[3]

    file = open("potsdam_"+str(adj)+"_"+method+"_generalization_2.txt",'w')
    
    potsdam_rgb = Image.open('./Data/Potsdam/top_potsdam_7_7_RGB.tif')
    potsdam_rgb = np.array(potsdam_rgb)
    potsdam_gray = np.round(potsdam_rgb[:,:,0]*0.3 + potsdam_rgb[:,:,1]*0.59 + potsdam_rgb[:,:,2]*0.11)
    
    potsdam_gray_train = np.array(potsdam_gray)[0:3000,:]
    d1_train = potsdam_gray_train.shape[0]
    d2_train = potsdam_gray_train.shape[1]
    
    potsdam_gray_test = np.array(potsdam_gray)[3000:,:]
    d1_test = potsdam_gray_test.shape[0]
    d2_test = potsdam_gray_test.shape[1]
    
    potsdam_label = Image.open('./Data/Potsdam/top_potsdam_7_7_label.tif')
    potsdam_label = np.array(potsdam_label)/255
    potsdam_label = potsdam_label[:,:,0] + 2*potsdam_label[:,:,1] + 4*potsdam_label[:,:,2]
    aux1 = potsdam_label == 6
    aux2 = potsdam_label == 7
    aux = aux1.astype(int)+aux2.astype(int)
    potsdam_label = potsdam_label-aux
    
    potsdam_label_test = potsdam_label[3000:,:]
    
    # Color map corresponding to the colors of the GT data
    #cmap = colors.ListedColormap([(0,0,0,1),(1,0,0,1),(0,1,0,1),(1,1,0,1),(0,0,1,1),(0,1,1,1),(1,1,1,1)])
    cmap = colors.ListedColormap([(1,0,0,1),(0,1,0,1),(1,1,0,1),(0,0,1,1),(0,1,1,1),(1,1,1,1)])
    unique,counts=np.unique(potsdam_label,return_counts=True)
    #print(unique,counts)
    
    cmap_ = colors.ListedColormap([(1,0,0,1),(0,1,0,1),(1,1,0,1),(0,0,1,1),(0,1,1,1),(1,1,1,1)])
    #plt.figure("GT")
    #plt.imshow(potsdam_label,cmap=cmap_)
    #plt.axis('off')
    #plt.show()
    
    ################################################################################################################
    ############################################ Random training set ###############################################
    ################################################################################################################
    
    random_potsdam_training = np.load(training_set)[0:3000,:]
    nb_samples_train = int(np.sum(random_potsdam_training>0))
    nb_samples_test = int(np.sum(potsdam_label_test>0))
    
    
    lamb_area=[200, 400, 800, 1600, 3200, 6400, 12800, 25600, 51200, 102400, 204800, 409600, 819200, 1638400]
    lamb_moi = [0.2, 0.3, 0.4, 0.5]
    
    ################################################################################################################
    start_time=time.time()
    
    if   (method == "GRAY"):
        features_train = np.zeros((1,d1_train,d2_train))
        features_train[0,:,:] = potsdam_gray_train
        features_test = np.zeros((1,d1_test,d2_test))
        features_test[0,:,:] = potsdam_gray_test  
    elif (method == "AP"):
        features_train=compute_AP(potsdam_gray_train, lamb_area, lamb_moi, adj)
        features_test=compute_AP(potsdam_gray_test, lamb_area, lamb_moi, adj)
    elif (method == "MAX"):
        features_train=compute_MAX(potsdam_gray_train, lamb_area, lamb_moi, adj)
        features_test=compute_MAX(potsdam_gray_test, lamb_area, lamb_moi, adj)
    elif (method == "MIN"):
        features_train=compute_MIN(potsdam_gray_train, lamb_area, lamb_moi, adj)
        features_test=compute_MIN(potsdam_gray_test, lamb_area, lamb_moi, adj)
    elif (method == "SDAP"):
        features_train=compute_SDAP(potsdam_gray_train, lamb_area, lamb_moi, adj)
        features_test=compute_SDAP(potsdam_gray_test, lamb_area, lamb_moi, adj)
    elif (method == "LFAP"):
        features_train=compute_LFAP(potsdam_gray_train, lamb_area, lamb_moi, adj)
        features_test=compute_LFAP(potsdam_gray_test, lamb_area, lamb_moi, adj)
    elif (method == "LFSDAP"):
        features_train=compute_LFSDAP(potsdam_gray_train, lamb_area, lamb_moi, adj)
        features_test=compute_LFSDAP(potsdam_gray_test, lamb_area, lamb_moi, adj)
    elif (method == "ALPHA"):
        features_train=compute_ALPHA(potsdam_gray_train, lamb_area, lamb_moi, adj)
        features_test=compute_ALPHA(potsdam_gray_test, lamb_area, lamb_moi, adj)
    elif (method == "OMEGA"):
        features_train=compute_OMEGA(potsdam_gray_train, lamb_area, lamb_moi, adj)
        features_test=compute_OMEGA(potsdam_gray_test, lamb_area, lamb_moi, adj)
    elif (method == "FP_AREA" or method == "FP_AREA_MEAN" or method == "FP_MEAN"):
        features_train=compute_FP(potsdam_gray_train, lamb_area, lamb_moi, adj, method)
        features_test=compute_FP(potsdam_gray_test, lamb_area, lamb_moi, adj, method)
    elif (method == "NO_THRESHOLD"):
        features_train=compute_NO_THRESHOLD(potsdam_gray_train,adj)
        features_test=compute_NO_THRESHOLD(potsdam_gray_test,adj)
    else:
        print("Method not implemented!")
        return
                    

    X_train = np.zeros((nb_samples_train,features_train.shape[0]))
    y_train = np.zeros((nb_samples_train))
    
    X_test = np.zeros((nb_samples_test,features_test.shape[0]))
    y_test = np.zeros((nb_samples_test))
    
    k = 0
    for i in range(0,d1_train):
        for j in range(0,d2_train):
            if (random_potsdam_training[i,j] != 0):
                X_train[k] = features_train[:,i,j]
                y_train[k] = potsdam_label[i,j]
                k=k+1
                
    k = 0
    for i in range(0,d1_test):
        for j in range(0,d2_test):
            if (potsdam_label_test[i,j] != 0):
                X_test[k] = features_test[:,i,j]
                y_test[k] = potsdam_label_test[i,j]
                k=k+1
 
    clf = RandomForestClassifier(n_estimators=100, random_state=0)
    clf.fit(X_train, y_train.ravel())
    label_classif = clf.predict(X_test)

    
    OCA,kappa,perclass,ACA,ConfMat = classificationEvaluation(y_test,label_classif)
    file.write("("+ str(y_train.size)+ " training samples, "+str(y_test.size)+"testing samples "+str(features_train.shape[0])+" features)"+"\n")
    file.write("OCA, ACA and Kappa: "+str(round(OCA*100,2))+" & "+str(round(ACA*100,2))+" & "+str(round(kappa,4))+"\n")
    file.write("Accuracy per class: " + str(round(perclass[0]*100,2)) + " & " + str(round(perclass[1]*100,2)) + " & " + 
                str(round(perclass[2]*100,2)) + " & " +str(round(perclass[3]*100,2)) + " & " + str(round(perclass[4]*100,2)) + " & " +\
                str(round(perclass[5]*100,2)) + "\n")
    
    
    print("("+ str(y_train.size)+ " training samples, "+str(y_test.size)+"testing samples)"+"\n")
    print("OCA, ACA and Kappa: "+str(round(OCA*100,2))+" & "+str(round(ACA*100,2))+" & "+str(round(kappa,4))+"\n")
    
    
    total_time = time.time()-start_time
    file.write("Total time to compute "+ str(total_time)+"\n")
    print("Total time to compute "+ str(total_time)+"\n")
    
    
    vis_test = np.zeros((d1_test,d2_test))
    kk = 0
    for i in range(0, d1_test):
        for j in range(0, d2_test):
            if (potsdam_label_test[i,j] != 0):
                vis_test[i,j] = label_classif[kk]
                kk = kk+1
    
    
    plt.imsave("classification_potsdam_"+str(adj)+"_"+method+"_generalization_2.png",vis_test,cmap=cmap)    
    
if __name__ == "__main__":
    main()
    
